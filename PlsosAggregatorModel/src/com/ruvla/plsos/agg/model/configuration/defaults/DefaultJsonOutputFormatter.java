package com.ruvla.plsos.agg.model.configuration.defaults;

import java.io.IOException;
import java.util.Arrays;
import java.util.Queue;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.ruvla.plsos.agg.model.PlsosConstants;
import com.ruvla.plsos.agg.model.datasend.OutputFormatter;
import com.ruvla.plsos.agg.model.dataview.AggDataUnit;
//import com.ruvla.plsos.collector.prototypes.android.AndroidConstants;

//import android.util.Log;

public class DefaultJsonOutputFormatter extends OutputFormatter {

	public DefaultJsonOutputFormatter() {
		super();
	}

	@Override
	public Object format(Queue<? extends AggDataUnit> unitList) {
		if (format.equalsIgnoreCase(PlsosConstants.FORMAT_JSON)) {
			// Log.d(AndroidConstants.LOG_TAG, "json formatting");
			ObjectMapper mapper = new ObjectMapper();
			String jsonOutput = null;
			try {
				// Log.d(AndroidConstants.LOG_TAG, "writting");
				jsonOutput = mapper.writeValueAsString(unitList);
			} catch (JsonGenerationException e) {
				jsonOutput = "json generation exception";
				e.printStackTrace();
			} catch (JsonMappingException e) {
				jsonOutput = "json mapping exception";
				e.printStackTrace();
			} catch (IOException e) {
				jsonOutput = "json IO exception";
				e.printStackTrace();
			}
			// Log.d(AndroidConstants.LOG_TAG, "returning " + jsonOutput);
			return jsonOutput;
		} else if (format.equalsIgnoreCase(PlsosConstants.FORMAT_RAW)) {
			return Arrays.deepToString(unitList.toArray());
		} else {
			return "Not supported by " + this.getClass().getSimpleName();
		}
	}

}
